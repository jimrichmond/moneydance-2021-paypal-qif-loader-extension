/***************************************************************************\
 *
 * Copyright (c) 2022, Jim Richmond
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
\***************************************************************************/

package com.moneydance.modules.features.paypalqifloader;

import com.moneydance.apps.md.controller.FeatureModule;
import com.moneydance.apps.md.controller.FeatureModuleContext;
import com.moneydance.apps.md.controller.ModuleUtil;
import com.moneydance.apps.md.controller.UserPreferences;
import com.infinitekind.util.CustomDateFormat;
import com.infinitekind.util.DateUtil;

import java.io.*;
import java.util.*;
import java.text.*;
import java.awt.*;

/* MoneyDance Pluggable module initialization
 * 
 * PaypalQIFLoader - Loads paypal QIF after doing some category/format fixup.
 * 
 *
*/

public class Main
extends FeatureModule
{
	private PaypalQIFLoaderWindow paypalQIFLoaderWindow = null;

	public static CustomDateFormat cdate;
	public static char decimalChar;
	public static FeatureModuleContext context;
	public static UserPreferences up; 
	public static MRBPreferences preferences;

	public void init() {
		// the first thing we will do is register this module to be invoked
		// via the application toolbar
		context = getContext();
		try {
			context.registerFeature(this, "showconsole",
			getIcon("pt_icon512.png"),
			getName());
		}
		catch (Exception e) {
			e.printStackTrace(System.err);
		}
		String strDateFormat;
		up = UserPreferences.getInstance();
		strDateFormat = up.getSetting(UserPreferences.DATE_FORMAT);
		cdate = new CustomDateFormat(strDateFormat);
		decimalChar = up.getDecimalChar();
		
	}

	public void cleanup() {
		closeConsole();
	}

	private Image getIcon(String action) {
		try {
			ClassLoader cl = getClass().getClassLoader();
			java.io.InputStream in = 
			cl.getResourceAsStream("/com/moneydance/modules/features/paypalqifloader/" + action);
			if (in != null) {
				ByteArrayOutputStream bout = new ByteArrayOutputStream();
				byte buf[] = new byte[1024];
				int n = 0;
				while((n=in.read(buf, 0, buf.length))>=0)
				bout.write(buf, 0, n);
				return Toolkit.getDefaultToolkit().createImage(bout.toByteArray());
			}
		} catch (Throwable e) { }
		return null;
	}

	/** Process an invokation of this module with the given URI */
	public void invoke(String uri) {
		String command = uri;
		String parameters = "";
		int theIdx = uri.indexOf('?');
		if(theIdx>=0) {
			command = uri.substring(0, theIdx);
			parameters = uri.substring(theIdx+1);
		}
		else {
			theIdx = uri.indexOf(':');
			if(theIdx>=0) {
				command = uri.substring(0, theIdx);
			}
		}

		if(command.equals("showconsole")) {
			if (preferences == null){
				MRBPreferences.loadPreferences(context);
				preferences = MRBPreferences.getInstance();
			}
			showConsole();
		}    
	}

	public String getName() {
		return "Paypal QIF Loader";
	}

	private synchronized void showConsole() {
		if(paypalQIFLoaderWindow==null) {
			paypalQIFLoaderWindow = new PaypalQIFLoaderWindow(this);
			paypalQIFLoaderWindow.setIconImage(getIcon("pt_icon512.png"));
			paypalQIFLoaderWindow.setVisible(true);
		}
		else {
			paypalQIFLoaderWindow.setVisible(true);
			paypalQIFLoaderWindow.toFront();
			paypalQIFLoaderWindow.requestFocus();
		}
	}

	FeatureModuleContext getUnprotectedContext() {
		return getContext();
	}

	synchronized void closeConsole() {
		if(paypalQIFLoaderWindow !=null) {
			paypalQIFLoaderWindow.goAway();
			paypalQIFLoaderWindow = null;
			System.gc();
		}
	}
}


