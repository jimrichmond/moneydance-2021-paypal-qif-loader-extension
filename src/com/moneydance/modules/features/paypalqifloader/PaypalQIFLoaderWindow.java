/***************************************************************************\
 * PaypalQIFLoaderWindow.java
 * Copyright (c) 2022, Jim Richmond
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
\***************************************************************************/

package com.moneydance.modules.features.paypalqifloader;

import com.infinitekind.moneydance.model.*;
import com.infinitekind.moneydance.model.SplitTxn;
import com.infinitekind.moneydance.model.ParentTxn;
import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.AccountUtil;
import com.infinitekind.util.CustomDateFormat;
import com.infinitekind.util.DateUtil;
import com.moneydance.apps.md.controller.Util;
import com.moneydance.awt.*;
import com.moneydance.awt.GridC;
import com.moneydance.awt.JDateField;

import org.apache.commons.lang3.*;

import java.io.*;
import java.util.*;
import java.text.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.TimeUnit;

/* PayPalQIFLoaderWindow
 * 
 * This class implements a QIF file transaction importer.
 *
 * The implementation is somewhat specific to the structure
 * of QIF files created by Paypal, although the tool could
 * potentially be used to import transactions from other similarly
 * formatted QIF files.
 *
 * Configuration - The configuration settings used for the import are
 *                 read from a text file called paypalqifloader.cfg that
 *                 must be storad in the same folder as the current
 *                 Moneydance data folder.
 *
 * (see paypalqifloader.cfg for more details on configuration options)
 *
 * Limitations
 *     - QIF file date format must be MM/DD/YYYY
 *     - QIF file amounts must be of the form 1,234.56 (minus signs and commas ok)
 *     - All QIF records must have at least 1 split and cannot have
 *       more than 2 splits.
 *
 */
public class PaypalQIFLoaderWindow extends JFrame {
	private Main extension;
	private AccountBook book;
  private Account root;
  
	private JProgressBar progressBar;
	private JLabel progressText;
	private JPanel topButtonPanel;
	private JButton analyzeButton;
	private JButton importButton;
	private JButton closeButton;
  private JTextArea statusTextArea;

  private AnalyzeImportThread analyzeImportThread;
  
  private int bytesProcessed;
  private int linesProcessed;
  private QIFRecordData qData;
  
  private class QIFRecordData {
      String dateString;
      int moneydanceDateInt;
      double totalAmount;
      String mainCategory;
      String firstSplitCategory;
      double firstSplitAmount;
      String secondSplitCategory;
      double secondSplitAmount;
      String memoString;
      String payeeString;
      boolean recordComplete;
      
   public QIFRecordData() {
     clear();
   }
   
   public void clear() {
     dateString = "";
     moneydanceDateInt = 0;
     totalAmount = Double.NaN;
     mainCategory = "";
     firstSplitCategory = "";
     firstSplitAmount = Double.NaN;
     secondSplitCategory = "";
     secondSplitAmount = Double.NaN;
     memoString = "";
     payeeString = "";
     recordComplete = false;
   }
   private int numAt(String str, int charPosition) {
     return ((int) str.charAt(charPosition) - (int) '0');
   }
   public int qifDateToDateInt(String qifDateString) {
   
     if (qifDateString.length() != 10) {
       statusTextArea.append("Error: QIF date (" + qifDateString + " is too short.  Should be MM/DD/YYYY format.\n");
       return 0;
     }
     if ((qifDateString.charAt(2) != '/') || (qifDateString.charAt(5) != '/')) {
       statusTextArea.append("Error: QIF date (" + qifDateString + " not formatted correctly.  Should be MM/DD/YYYY format.\n");
       return 0;       
     }
     String str = qifDateString.replaceAll("/","");
     try {
       double d = Double.parseDouble(str);
     } catch (Exception e) {
       statusTextArea.append("Error: QIF date (" + qifDateString + " not formatted correctly.  Should be MM/DD/YYYY format.\n");
       return 0;      
     }
     
     return 
     // 01234567
     // MMDDYYYY -> YYYYMMDD
                   (10000000*numAt(str,4)) +
                    (1000000*numAt(str,5)) +
                     (100000*numAt(str,6)) +
                      (10000*numAt(str,7)) +
                       (1000*numAt(str,0)) +
                        (100*numAt(str,1)) +
                         (10*numAt(str,2)) +
                          (1*numAt(str,3));                     
   }
   
   public String toString() {
       return "Record: D=" + dateString + " T=" + totalAmount +
                " L=" + mainCategory + " S1=" + firstSplitCategory + " S1$=" + firstSplitAmount + 
                " S2=" + secondSplitCategory + " S2$=" + secondSplitAmount + " M=" + memoString +
                " P=" + payeeString + "\n";
   }
   
  } // class QIFRecordData
    
  private class CategoryReplacementInfo {
    String qIFCategory;
    String moneydanceCategory;
    String defaultPayee;
    double minAmount;
    double maxAmount;
      
    public CategoryReplacementInfo() {
        qIFCategory = "";
        moneydanceCategory = "";
        defaultPayee = "";
        minAmount = -Double.MAX_VALUE;
        maxAmount = Double.MAX_VALUE;        
    }
    public String toString() {
    
    String minStr;
    String maxStr;
    if (minAmount == -Double.MAX_VALUE)
      minStr = "-MAX_VALUE";
    else
      minStr = getCurrencyString(minAmount);
    
    if (maxAmount == Double.MAX_VALUE)
      maxStr = "MAX_VALUE";
    else
      maxStr = getCurrencyString(maxAmount);
      
    return "qIFCategory=" + qIFCategory + 
                ", moneydanceCategory=" + moneydanceCategory +
                "\ndefaultPayee=" + defaultPayee + 
                ", minAmount=" + minStr + 
                ", maxAmount=" + maxStr + "\n";
    }
    
    public boolean isMatch(String qIFCatIn, double amount) {
        if (qIFCategory.equals(qIFCatIn) && (amount >= minAmount) && (amount <= maxAmount))
          return true;
        else
          return false;
    }      
  } // class CategoryReplacementInfo
  
	private class ConfigInfo {
    String qIFFilename;
    String moneydanceAccountName;
    String defaultCategory;
    boolean saveQIFCategoryToMemoIfDefaulted;
    boolean skipZeroAmountSplits;
    boolean reverseSignOnAmounts;
    boolean markTransactionsAsNew;
    boolean loadSuccessful;
    boolean verboseOutputMode;

	  ArrayList < CategoryReplacementInfo > categoryReplacementList = new ArrayList < CategoryReplacementInfo > ();    
        
		public ConfigInfo(String filename) {
    qIFFilename = "";
    moneydanceAccountName = "";
    defaultCategory = "";
    saveQIFCategoryToMemoIfDefaulted = false;
    skipZeroAmountSplits = false;
    reverseSignOnAmounts = false;
    markTransactionsAsNew = false;
    verboseOutputMode=false;        
    loadSuccessful = false;
    String line = null;
    CategoryReplacementInfo categoryInfo = new CategoryReplacementInfo();
    boolean categoryRecordInProgress = false;
    int configLinesProcessed = 0;  
    
    FileInputStream fstream = null;
	  DataInputStream in = null;
	  BufferedReader br = null;
		try {
			fstream = new FileInputStream(filename);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));

      while ((line = br.readLine()) != null) {
          configLinesProcessed++;
          if (line.charAt(0) == ';') { // skip comment lines
          } else if (line.contains("InputFile"))
             qIFFilename = line.substring(line.indexOf('=')+1);
          else if (line.contains("MoneydanceAccountName"))
             moneydanceAccountName = line.substring(line.indexOf('=')+1);
          else if (line.contains("DefaultMoneydanceCategory"))
             defaultCategory = line.substring(line.indexOf('=')+1);
          else if (line.contains("SaveQIFCategoryToMemoIfDefaulted"))
             saveQIFCategoryToMemoIfDefaulted = Boolean.parseBoolean(line.substring(line.indexOf('=')+1));
          else if (line.contains("SkipZeroAmountSplits"))
             skipZeroAmountSplits = Boolean.parseBoolean(line.substring(line.indexOf('=')+1));
          else if (line.contains("MarkTransactionsAsNew"))
             markTransactionsAsNew = Boolean.parseBoolean(line.substring(line.indexOf('=')+1));
          else if (line.contains("ReverseSignOnAmounts"))
             reverseSignOnAmounts = Boolean.parseBoolean(line.substring(line.indexOf('=')+1));
          else if (line.contains("VerboseOutputMode"))
             verboseOutputMode = Boolean.parseBoolean(line.substring(line.indexOf('=')+1));
          else if (line.contains("QIFCategory"))
             categoryInfo.qIFCategory = line.substring(line.indexOf('=')+1);
          else if (line.contains("MoneydanceCategory"))
             categoryInfo.moneydanceCategory = line.substring(line.indexOf('=')+1);             
          else if (line.contains("DefaultPayee"))
             categoryInfo.defaultPayee = line.substring(line.indexOf('=')+1); 
          else if (line.contains("MinAmount"))
             categoryInfo.minAmount = Double.parseDouble(line.substring(line.indexOf('=')+1));
          else if (line.contains("MaxAmount"))
             categoryInfo.maxAmount = Double.parseDouble(line.substring(line.indexOf('=')+1));
          else if (line.contains("Category")) {
             if (categoryRecordInProgress == true) {
               statusTextArea.append("Error: Incomplete Category record near line " + 
                                         configLinesProcessed + ".\n");
               break;
             }
             categoryInfo = new CategoryReplacementInfo();
             categoryRecordInProgress = true;
          } else if (line.contains("End")) {
             categoryReplacementList.add(categoryInfo);
             categoryRecordInProgress = false;
          }
         } // end while
      loadSuccessful = true;  
			in.close();
		} catch (Exception e) { // Catch exception if any
      if (br == null) {
			  statusTextArea.append("Error: Unable to open configuration file: " + filename + "\n");
        statusTextArea.append("       Creating default configuration file - BE SURE TO CUSTOMIZE BEFORE ATTEMPTING IMPORT.\n");
        createDefaultConfigFile(filename);
      } else {
			  statusTextArea.append("Error opening or reading config file: " + filename + " near line " + configLinesProcessed + ".\n");
        statusTextArea.append("Exception Message: " + e.toString() + "\n");
        if (line != null)
          statusTextArea.append("Last QIF Line: " + line + "\n");
        if (categoryRecordInProgress == true) 
          statusTextArea.append("In Progress Record: " + categoryInfo.toString() + "\n");
      }  
		}
   } // Constructor - ConfigInfo()
   
   private void createDefaultConfigFile(String filename) {
  
    File f = new File(filename);
    if (f.exists()) {
      statusTextArea.append("Error creating sample configuration file: File already exists.\n");
      return;
    }
    
		try {
	    FileOutputStream fos = new FileOutputStream(f);
	    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
 
		  bw.write("; Configuration for Moneydance Paypal QIF Loader Extension\n");
		  bw.write(";\n");
		  bw.write("; Put this file in the same folder as the current Moneydance data file.\n");
		  bw.write("; The Paypal QIF Loader extension will automatically find it.\n");
		  bw.write(";\n");
		  bw.write("; More documentation can be found at the end of this file (scroll down).\n");
		  bw.write(";\n");
		  bw.write("InputFile=C:\\Users\\Test\\Downloads\\Download.QIF\n");
		  bw.write("MoneydanceAccountName=Paypal Account\n");
		  bw.write("DefaultMoneydanceCategory=Temp Import Category\n");
		  bw.write("SaveQIFCategoryToMemoIfDefaulted=true\n");
		  bw.write("SkipZeroAmountSplits=true\n");
		  bw.write("MarkTransactionsAsNew=false\n");
		  bw.write("ReverseSignOnAmounts=true\n");
		  bw.write("VerboseOutputMode=false\n");
		  bw.write("Category\n");
		  bw.write("  QIFCategory=Fee\n");
		  bw.write("  MoneydanceCategory=Paypal Transaction Fees\n");
		  bw.write("End\n");
		  bw.write("Category\n");
		  bw.write("  QIFCategory=General Withdrawal\n");
		  bw.write("  MoneydanceCategory=Owner's Draw\n");
		  bw.write("  DefaultPayee=Business Owner Withdrawal\n");
		  bw.write("End\n");
		  bw.write("Category\n");
		  bw.write("  QIFCategory=Website Payment\n");
		  bw.write("  MoneydanceCategory=Misc Revenue - Small\n");
		  bw.write("  MinAmount=0\n");
		  bw.write("  MaxAmount=100\n");
		  bw.write("End\n");
		  bw.write("Category\n");
		  bw.write("  QIFCategory=Website Payment\n");
		  bw.write("  MoneydanceCategory=Misc Revenue - Large\n");
		  bw.write("  MinAmount=100\n");
		  bw.write("  MaxAmount=1000\n");
		  bw.write("End\n");
		  bw.write(";\n");
		  bw.write("; Documentation (continued)\n");
		  bw.write(";\n");
		  bw.write("; This extension is somewhat specific to the structure\n");
		  bw.write("; of QIF files created by Paypal, although the tool could\n");
		  bw.write("; potentially be used to import transactions from other similarly\n");
		  bw.write("; formatted QIF files.\n");
		  bw.write("; \n");
		  bw.write("; The entries above control the pre-import processing done to QIF transactions\n");
		  bw.write("; prior to importing them.  The main processing is to replace the QIF\n");
		  bw.write("; categories (L and S tag records) with appropriate Moneydance categories. \n");
		  bw.write("; \n");
		  bw.write("; 'Category' entries above create a mapping between QIF categories and Moneydance\n");
		  bw.write("; categories. Any categories found in the QIF file that aren't matched with\n");
		  bw.write("; a 'Category' entry will be replaced with the default category.  No new moneydance\n");
		  bw.write("; accounts or categories are created during the import process.  A catch-all\n");
		  bw.write("; category should be created in moneydance to handle any unexpected categories\n");
		  bw.write("; (ex. Temp Import Category).  After the import, go to this category to review\n");
		  bw.write("; and manually process any imported transactions that defaulted to this category\n");
		  bw.write(";\n");
		  bw.write("; A default payee (moneydance description) can be specified in any Category\n");
		  bw.write("; record and if a QIF transaction matching that category has a blank payee\n");
		  bw.write("; the default will be used.\n");
		  bw.write(";\n");
		  bw.write("; If SaveQIFCategoryToMemoIfDefaulted=true, the QIF category will be prepended\n");
		  bw.write("; to any text in the QIF memo line, separated by a '-'\n");
		  bw.write(";\n");
 		  bw.write("; If MarkTransactionsAsNew=true, the new flag is set on all imported transactions.\n");
		  bw.write("; This activates Moneydance logic that allows each imported transaction to be\n");
		  bw.write("; manually reviewed, edited, and confirmed.\n");
		  bw.write(";\n");
		  bw.write("; If ReverseSignOnAmounts=true, amounts in the QIF file have their sign changed.\n");
		  bw.write("; This can be used to correct mismatches in how signs are handled in the QIF\n");
		  bw.write("; file or the Moneydance account the transactions are imported into.\n");
		  bw.write(";\n");      
		  bw.write("; If SkipZeroAmountSplits=true, second splits in a QIF transaction record will be\n");
		  bw.write("; ignored if the amount of the second split is zero.  This eliminates unneccesary\n");
		  bw.write("; splits in the imported moneydance transactions.\n");
		  bw.write(";\n");
		  bw.write("; Category entries may include an optional min and max transaction amount\n");
		  bw.write("; which becomes part of the match criteria.  This allows the same paypal category\n");
		  bw.write("; to be matched with different moneydance categories depending on the amount.\n");
		  bw.write(";\n");
		  bw.write(";  Limitations\n");
		  bw.write(";      - QIF file date format must be MM/DD/YYYY\n");
		  bw.write(";      - QIF file amounts must be of the form 1,234.56 (minus signs and commas ok)\n");
		  bw.write(";      - All QIF records must have at least 1 split and cannot have\n");
		  bw.write(";        more than 2 splits.\n");
		  bw.write(";\n");
	    bw.close();
		} catch (Exception e) { // Catch exception if any
			  statusTextArea.append("Error: Unable to write configuration file: " + filename + " for writing.  Aborting.\n");
		}
   } // method createDefaultConfigFile()

   private CategoryReplacementInfo getCategoryReplacementInfo(String qifCategory, double amount) {
   
		 for (int i=0; i<(categoryReplacementList.size()); i++) 
	    if (categoryReplacementList.get(i).isMatch(qifCategory, amount))
        return categoryReplacementList.get(i);
     return null;
   } // method ConfigInfo.getCategoryReplacementInfo()
   
   void dumpToStatusTextArea() {
    
    statusTextArea.append("qIFFilename=" + qIFFilename + 
        ", moneydanceAccountName=" + moneydanceAccountName +
        "\ndefaultCategory=" + defaultCategory + 
        ", saveQIFCategoryToMemoIfDefaulted="+String.valueOf(saveQIFCategoryToMemoIfDefaulted) +
        ", reverseSignOnAmounts=" + String.valueOf(reverseSignOnAmounts) + 
        ", markTransactionsAsNew="+String.valueOf(markTransactionsAsNew) +
        ", skipZeroAmountSplits=" + String.valueOf(skipZeroAmountSplits) + 
        ", verboseOutputMode=" +  String.valueOf(verboseOutputMode) +"\n");
		for (int i=0; i<(categoryReplacementList.size()); i++) 
				statusTextArea.append(categoryReplacementList.get(i).toString());     
    }
	} // class ConfigInfo

	private class AnalyzeImportThread extends Thread {
		boolean analyzeNeeded = false;
		boolean importNeeded = false;
		boolean terminate = false;
		AnalyzeImportThread() {}@Override public void run() {
			while (terminate == false) {
				while ((analyzeNeeded == false) && (importNeeded == false)) {
					try {
						this.sleep(100);
					} catch(InterruptedException e) {
						e.printStackTrace();
					}
					if (terminate == true)
						return;
				}
				if ((analyzeNeeded == true) || (importNeeded == true)){
					topButtonPanel.setVisible(false);
					progressBar.setValue(0);
					progressBar.setVisible(true);
					progressText.setVisible(true);
					doAnalyzeImport(book, importNeeded);
					progressBar.setVisible(false);
					progressText.setVisible(false);
					topButtonPanel.setVisible(true);
          analyzeNeeded = false;
          importNeeded = false;
					topButtonPanel.repaint();
				}
			}
		}
	} // class AnalyzeImportThread
  
	public PaypalQIFLoaderWindow(Main extension) {
  
		super("Moneydance Paypal QIF Loader Extension - " + extension.getUnprotectedContext().getCurrentAccountBook().getName());
		this.extension = extension;
		book = extension.getUnprotectedContext().getCurrentAccountBook();
    root = extension.getUnprotectedContext().getRootAccount();
    
		analyzeImportThread = new AnalyzeImportThread();
    qData = new QIFRecordData();
    
		analyzeButton = new JButton("Analyze");
		analyzeButton.setToolTipText("Process QIF file and update table but don't import");
		analyzeButton.setFocusable(false);
		analyzeButton.addActionListener(new ActionListener() {
		@Override public void actionPerformed(ActionEvent e) {
				analyzeImportThread.analyzeNeeded = true;
			}
		});

		importButton = new JButton("Import");
		importButton.setToolTipText("Perform QIF import and load transactions into Moneydance");
		importButton.setFocusable(false);
		importButton.addActionListener(new ActionListener() {
		@Override public void actionPerformed(ActionEvent e) {
				analyzeImportThread.importNeeded = true;
			}
		});

		closeButton = new JButton("Close");
		closeButton.setToolTipText("Close Paypal QIF Loader");
		closeButton.setFocusable(false);
		closeButton.addActionListener(new ActionListener() {
		@Override public void actionPerformed(ActionEvent e) {
				savePreferences();
				extension.closeConsole();
			}
		});
    
		topButtonPanel = new JPanel();
		topButtonPanel.add(analyzeButton);
		topButtonPanel.add(importButton);
		topButtonPanel.add(closeButton);

		JPanel progressBarPanel = new JPanel();
		progressBarPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		progressBar = new JProgressBar(0, 100);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		progressBar.setVisible(false);
		progressText = new JLabel("Processing QIF File...");
		progressText.setFont(progressText.getFont().deriveFont(Font.BOLD));
		progressText.setVisible(false);
		progressBarPanel.add(progressText);
		progressBarPanel.add(progressBar);
    
    statusTextArea = new JTextArea();
    statusTextArea.setEditable(false); // set textArea non-editable
    JScrollPane textAreaWithScroll = new JScrollPane(statusTextArea);
    textAreaWithScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		JPanel p = new JPanel(new GridBagLayout()); // x,y, wtx,wty, spanx, spany fillx, filly //  top, left, bottom, right
		p.setBorder(new EmptyBorder(10, 10, 10, 10));
		p.add(topButtonPanel,  AwtUtil.getConstraints(2, 0, 0, 0, 2, 1, false, false, GridBagConstraints.EAST, 0, 0, 2, 0));
		p.add(progressBarPanel,AwtUtil.getConstraints(2, 0, 0, 0, 2, 1, true, false, GridBagConstraints.EAST, 4, 0, 4, 0));
		p.add(textAreaWithScroll, AwtUtil.getConstraints(0, 1, 1, 1, 4, 1, true, true));
		getContentPane().add(p);

		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		enableEvents(WindowEvent.WINDOW_CLOSING);
		analyzeImportThread.start();
		if (book != null) {
			analyzeImportThread.analyzeNeeded = true;
		}
		
		this.pack();

		setSize(Main.preferences.getInt("PaypalQIFLoader.FrameWidth", 1200), Main.preferences.getInt("PaypalQIFLoader.FrameHeight", 500));
		AwtUtil.centerWindow(this);
	} // method PaypalQIFLoaderWindow
     
  private String getNextQIFLine(BufferedReader br, String errorString) {
  String line;
   try {
    if ((line = br.readLine()) == null) {
      statusTextArea.append("Unexpected QIF EOF encountered " + errorString + " near line: " + linesProcessed + "\n");
    }
    linesProcessed++;
    bytesProcessed+=line.length();
   } catch (Exception e) {
    line = null;
	  statusTextArea.append("Error - Exception reading QIF file near line " + linesProcessed + ".\n");
    statusTextArea.append("Exception Message: " + e.toString() + "\n");
   }
    return line;
  } // method getNextQIFLine()
  
  private boolean validateQIFLine(String line, String compareStr, String errorStr, int linesProcessed) {
     boolean ok = false;
     if (line.startsWith(compareStr) != true)
        statusTextArea.append("Unexpected QIF input, " + errorStr + " near line: " + linesProcessed + ", encountered: " + line  + "\n");
     else
       ok = true;
     return ok;
  }  
  
  private void doQdataFixup(ConfigInfo config) {
    CategoryReplacementInfo crInfo;
    
    // Handle fixup for main category
    crInfo = config.getCategoryReplacementInfo(qData.mainCategory, qData.totalAmount);
    if (crInfo != null) { // Found category match, use replacement
      if (qData.payeeString.length() == 0)
        qData.payeeString = crInfo.defaultPayee;
      qData.mainCategory = crInfo.moneydanceCategory;
    }
    else { // No category match, use default category
      if (config.saveQIFCategoryToMemoIfDefaulted == true)
        qData.memoString = qData.mainCategory + "-" + qData.memoString;
      qData.mainCategory = config.defaultCategory;
    }

    // Handle fixup for first split
    if (qData.firstSplitAmount != Double.NaN) {
     crInfo = config.getCategoryReplacementInfo(qData.firstSplitCategory,qData.firstSplitAmount);
     if (crInfo != null) { // Found category match, use replacement
       qData.firstSplitCategory = crInfo.moneydanceCategory;
     }
     else { // No category match, use default category
       if (config.saveQIFCategoryToMemoIfDefaulted == true)
         qData.memoString = qData.firstSplitCategory + "-" + qData.memoString;
       qData.firstSplitCategory = config.defaultCategory;
     }
    } 

    // Handle fixup for second split
    if (qData.secondSplitAmount != Double.NaN) {
     crInfo = config.getCategoryReplacementInfo(qData.secondSplitCategory,qData.secondSplitAmount);
     if (crInfo != null) { // Found category match, use replacement
       qData.secondSplitCategory = crInfo.moneydanceCategory;
     }
     else { // No category match, use default category
       if (config.saveQIFCategoryToMemoIfDefaulted == true)
         qData.memoString = qData.secondSplitCategory + "-" + qData.memoString;
       qData.secondSplitCategory = config.defaultCategory;
     }
    }
    
  } // method fixupQIFInfo()
   
  private boolean saveTransactionToMoneydance(ConfigInfo config, QIFRecordData qData) {
  
    Account acct = null;
    Account mainCategory = null;
    Account firstSplitCategory = null;
    Account secondSplitCategory = null;
    boolean useSecondSplit = false;

    if ((qData.secondSplitAmount != Double.NaN) &&
        ((qData.secondSplitAmount != 0) || (config.skipZeroAmountSplits == false)))
      useSecondSplit = true;

    // Get Moneydance accounts/categories as needed
    acct = root.getAccountByName(config.moneydanceAccountName);
    if (acct == null)
      statusTextArea.append("Error: Unable to find account " + config.moneydanceAccountName + " in Moneydance account book.  Check paypalqifloader.cfg for typos.\n");
      
    mainCategory = root.getAccountByName(qData.mainCategory);
    if (mainCategory == null)
      statusTextArea.append("Error: Unable to find category " + qData.mainCategory + " in Moneydance accound book.  Check paypalqifloader.cfg for typos.\n");   
    if ((qData.firstSplitAmount != Double.NaN) && (qData.firstSplitCategory.length() != 0))
      firstSplitCategory = root.getAccountByName(qData.firstSplitCategory);
    if (firstSplitCategory == null)
      statusTextArea.append("Error: Unable to find category " + qData.firstSplitCategory + " in Moneydance accound book.  Check paypalqifloader.cfg for typos.\n");  
    if (useSecondSplit == true) {
      secondSplitCategory = root.getAccountByName(qData.secondSplitCategory);
      if (secondSplitCategory == null)
      statusTextArea.append("Error: Unable to find split category " + qData.secondSplitCategory + " in Moneydance accound book.  Check paypalqifloader.cfg for typos.\n");    
    }  
    if ((acct == null) || (mainCategory == null) || (firstSplitCategory == null) ||
       ((secondSplitCategory == null) && (useSecondSplit == true))) 
      return false;
    
    ParentTxn pTxn = ParentTxn.makeParentTxn(book, qData.moneydanceDateInt, qData.moneydanceDateInt, 
                      0, //DateUtil.convertIntDateToLong(qData.moneydanceDateInt), 
                      "", acct, qData.payeeString, qData.memoString, -1, 
                      AbstractTxn.STATUS_UNRECONCILED);
                      
    long longAmount = Double.valueOf(Math.round(qData.firstSplitAmount*100.0)).longValue();
    if (config.reverseSignOnAmounts == true)
       longAmount = -longAmount;
       
    SplitTxn spTxn = SplitTxn.makeSplitTxn( pTxn, longAmount, longAmount, 
                        1.0, firstSplitCategory, "", -1, AbstractTxn.STATUS_UNRECONCILED);
        
    if (config.markTransactionsAsNew==true)
       spTxn.setIsNew( true );
    pTxn.addSplit( spTxn );
      
    if ((secondSplitCategory != null) && ((qData.secondSplitAmount != 0) || (config.skipZeroAmountSplits == false))) {
      longAmount = Double.valueOf(Math.round(qData.secondSplitAmount*100.0)).longValue();
      if (config.reverseSignOnAmounts == true)
         longAmount = -longAmount;
      SplitTxn spTxn2 = SplitTxn.makeSplitTxn( pTxn, longAmount, longAmount, 1.0, 
                         secondSplitCategory, "", -1, AbstractTxn.STATUS_UNRECONCILED);
                         
      if (config.markTransactionsAsNew==true)
        spTxn.setIsNew( true );
      pTxn.addSplit( spTxn2 );
    }

    if (config.markTransactionsAsNew==true)
      pTxn.setIsNew( true );
      
    pTxn.syncItem();

    return true; 
  }
  
	private void doAnalyzeImport(AccountBook book, boolean importNeeded) {
    boolean analyzeOnly = !importNeeded;
    
    statusTextArea.setText(""); // clear the text area

    String filepath = book.getRootFolder().getParent();
    if (analyzeOnly == true)
       statusTextArea.append("Loading config file: " + filepath + "\\paypalqifloader.cfg\n");
    ConfigInfo config = new ConfigInfo(filepath + "\\paypalqifloader.cfg");
    if (config.loadSuccessful != true)
      return;
  
    if ((config.verboseOutputMode == true) && (analyzeOnly == true))
       config.dumpToStatusTextArea();
    if (analyzeOnly == true) {
		  statusTextArea.append("Found " + config.categoryReplacementList.size() + " category replacement records in configuration file.\n");
      statusTextArea.append("Configured Moneydance import account: " + config.moneydanceAccountName + ".\n");
    }
    
    String filename = config.qIFFilename;
    if (importNeeded == true)
      statusTextArea.append("IMPORTING TRANSACTIONS FROM QIF FILE: " + filename + "\n");
    else
      statusTextArea.append("Analyzing QIF File: " + filename + " (no transactions will be saved)\n");
    
    String line = null;
    int qifRecordCount = 0;
    boolean qifRecordInProgress = false;
    boolean error = false;
    bytesProcessed = 0;
    linesProcessed = 0;    
    qData.clear();
    
	  try {
			FileInputStream fstream = new FileInputStream(filename);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
      progressBar.setMaximum((int) fstream.getChannel().size()); 

      
      while ((line = br.readLine()) != null) {
          error = true;
          progressBar.setValue(bytesProcessed);
          linesProcessed++;
          bytesProcessed += line.length();
          if (line.contains("!Type:")) {
            // Start of new transaction
            if (qifRecordInProgress == true)  {
              statusTextArea.append("Error: Start of transaction before last record was finished around line " + linesProcessed + "\n");
              statusTextArea.append("       In-progress transaction: " + qData.toString());   
              break;
            }
            
            qData.clear();
            qifRecordInProgress = true;
            
            // Extract Date String
            if ((line = getNextQIFLine(br, "after !Type")) == null)
              break;
            if (validateQIFLine(line, "D", "looking for Date", linesProcessed) == false)          
              break;           
            qData.dateString = line.substring(1);
            qData.moneydanceDateInt = qData.qifDateToDateInt(qData.dateString);
            if (qData.moneydanceDateInt == 0) {
              statusTextArea.append("Error converting QIF date to Moneydance date around QIF file line " + linesProcessed + ".\n");
              break;
            }
            
            // Extract Transaction Total
            if ((line = getNextQIFLine(br, "after Date")) == null)
              break;
            if (validateQIFLine(line, "T", "looking for T", linesProcessed) == false)          
              break;
            qData.totalAmount = Double.parseDouble(line.substring(1).replaceAll(",",""));

            // Extract Main category
            if ((line = getNextQIFLine(br, "after T")) == null)
              break;
            if (validateQIFLine(line, "L", "looking for L", linesProcessed) == false)          
              break;           
            qData.mainCategory = line.substring(1);

            // Extract first split category
            if ((line = getNextQIFLine(br, "after L")) == null)
              break;
            if (validateQIFLine(line, "S", "looking for first S", linesProcessed) == false)          
              break;           
            qData.firstSplitCategory = line.substring(1);

            // Extract first split amount
            if ((line = getNextQIFLine(br, "after first S")) == null)
              break;
            if (validateQIFLine(line, "$", "looking for first split $", linesProcessed) == false)
              break;            
            qData.firstSplitAmount = Double.parseDouble(line.substring(1).replaceAll(",",""));   

            // Extract second split category and amount (if present)
            if ((line = getNextQIFLine(br, "after first split $")) == null)
              break;        
            if (line.startsWith("S") == true) { // Second split (fee?) present
              qData.secondSplitCategory = line.substring(1);
              if ((line = getNextQIFLine(br, "after second S")) == null)
                break;
              if (validateQIFLine(line, "$", "looking for second split $", linesProcessed) == false)
                break;
              qData.secondSplitAmount = Double.parseDouble(line.substring(1).replaceAll(",",""));

              if ((line = getNextQIFLine(br, "after second $")) == null)
                break;              
            }
            
            // Check for CX, which we'll skip over if present
            if (line.startsWith("CX") == true) {
              if ((line = getNextQIFLine(br, "after CX")) == null)
                break;         
            }
            
            // Check for Memo
            if (line.startsWith("M") == true) {
              qData.memoString = line.substring(1);
              if ((line = getNextQIFLine(br, "after M")) == null)
                break;         
            }
            // Check for Payee
            if (line.startsWith("P") == true) {
              qData.payeeString = line.substring(1);
              if ((line = getNextQIFLine(br, "after P")) == null)
                break;          
            }
            // Check for end of record
            if (line.startsWith("^") == true) {
               if ((config.verboseOutputMode == true) && (analyzeOnly == true))
                  statusTextArea.append("Orig:   " + qData.toString());
               qifRecordCount++;
               qifRecordInProgress = false;
               doQdataFixup(config);
               if (analyzeOnly == true)
                  statusTextArea.append("Fixed: " + qData.toString());

               // Import the completed QIF record into Moneydance
               if (importNeeded == true) {
                 if (saveTransactionToMoneydance(config, qData) == false) {
                    statusTextArea.append("Import aborted early due to error near QIF file line " + linesProcessed + "\n");
                    break;
                 }
                 statusTextArea.append("Imported: " + qData.toString());
               }
               error = false; // processed record successfully
            }             
          } // if line contains !Type
        } // while not EOF
      if (error == false)
        if (importNeeded == true)
         statusTextArea.append("Import completed without errors.  Imported " + qifRecordCount + "  transactions into Moneydance account " + config.moneydanceAccountName + ".\n");
        else
         statusTextArea.append("Analysis completed without errors.  Processed " + qifRecordCount + " QIF transaction records.\n");
			in.close();
     } catch (Exception e) { // Catch exception if any
			statusTextArea.append("Error - Exception opening or reading QIF file " + filename + " near line " + linesProcessed + ".\n");
      statusTextArea.append("Exception Message: " + e.toString() + "\n");
      if (line != null)
         statusTextArea.append("Last QIF Line: " + line + "\n");
      if (qifRecordInProgress == true) 
         statusTextArea.append("In Progress Record: " + qData.toString() + "\n");
	   }
     
	}

	private String getCurrencyString(double val) {
		NumberFormat cf = NumberFormat.getCurrencyInstance();
		cf.setMinimumFractionDigits(2);
		cf.setMaximumFractionDigits(2);
		return cf.format(val);
	}

	public final void processEvent(AWTEvent evt) {
		if (evt.getID() == WindowEvent.WINDOW_CLOSING) {
			savePreferences();
			extension.closeConsole();
			return;
		}
		if (evt.getID() == WindowEvent.WINDOW_OPENED) {}
		super.processEvent(evt);
	}

	private void savePreferences() {
		Dimension frameDimension = this.getSize();
		Main.preferences.put("PaypalQIFLoader.FrameWidth", frameDimension.width);
		Main.preferences.put("PaypalQIFLoader.FrameHeight", frameDimension.height);
		Main.preferences.isDirty();
	}

	void goAway() {
		savePreferences();
		setVisible(false);
		analyzeImportThread.terminate = true;
		dispose();
	}
}